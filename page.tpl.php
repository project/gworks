<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">
<head>
<title><?php print $head_title ?></title>
<meta http-equiv="Content-Style-Type" content="text/css" />
<?php print $head ?><?php print $styles ?>
  <script language="javascript">
    function preload(imgObj,imgSrc) {
      if (document.images) {
        eval(imgObj+' = new Image()')
        eval(imgObj+'.src = "themes/gworks/images/'+imgSrc+'"')
      }
    }
    function switchImg(elt,str){
      elt.style.backgroundImage="url(themes/gworks/images/"+str+".png)";
    }
    preload('tab_on','tab_on.png');
    preload('tab','tab.png');
  </script>
</head>
<body <?php print theme("onload_attribute"); ?>> 
<table width="740px" cellspacing="0px" cellpadding="0px" align="left" border="0px">
   <tr>
    <td colspan="3">
      <table cellpadding="0px" cellspacing="0px" border="0px">
        <tr>
          <td><img src="themes/gworks/images/banner.png"/></td>
        </tr>
        <tr> 
          <td valign="top" height="23px">
            <table cellpadding="0px" cellspacing="0px" width="740px">
              <tr>
                <td width="1px"></td>
                <td width="738px" style="background-image: url(themes/gworks/images/tab.png);background-repeat:x-repeat;">
                  <div id="top-nav">
                    <?php if (is_array($primary_links)) : ?> 
                    
                    <?php print "<table cellpadding=\"0px\" cellspacing=\"0px\"><tr>"; ?>
      <?php foreach ($primary_links as $link): ?> 
      
      <?php print "<td class=\"buttons\" style=\"background-image: url(themes/gworks/images/tab.png);background-repeat:no-repeat;\" height=\"23px\" width=\"123px\" onmouseover=\"switchImg(this,'tab_on')\" onmouseout=\"switchImg(this,'tab')\">$link</td>"; ?>
      
      <?php endforeach; ?> 
            <?php print "</tr></table>"; ?>
    <?php endif; ?>
                </div>
                
                </td>
                <td width="1px"></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td id="sidebar-left">
      <div id="menu">


  <?php if ((sidebar_left != "")  || ($sidebar_left != "")): ?>
    <div class="sidebar" id="sidebar-left">
    <?php print $sidebar_left ?> <?php print $sidebar_right ?>
    </div>
  <?php endif; ?> 


    <?php if ($search_box): ?> 
    <form action="<?php print $search_url ?>" method="post" id="searchform"> 
      <fieldset>
       <input type="text" value="" name="keys" id="s" /> 
       <input type="submit" value="Go!" id="searchbutton" /> 
      </fieldset>
    </form> 
    <?php endif; ?>


      </div>
    </td>
    <td style="width:1%; background-image: url(themes/gworks/images/vline.png);margin-top:10px;margin-bottom:10px;">
          <img src="themes/gworks/images/dot.png" alt="" style="align:middle; vertical-align:top; width:2px; height:1px; border:0px" />
    </td>
    <td width="590px" style="vertical-align:top;padding:7px;">
    <div id="content"> 
    <div class="navigation"> <?php print $breadcrumb ?> </div> 
    <?php if ($messages != ""): ?> 
    <div id="message"><?php print $messages ?></div> 
    <?php endif; ?> 
    <?php if ($mission != ""): ?> 
    <div id="mission"><?php print $mission ?></div> 
    <?php endif; ?> 
    <?php if ($title != ""): ?> 
    <h2 class="page-title"><?php print $title ?></h2> 
    <?php endif; ?> 
    <?php if ($tabs != ""): ?> 
    <?php print $tabs ?> 
    <?php endif; ?> 
    <?php if ($help != ""): ?> 
    <p id="help"><?php print $help ?></p> 
    <?php endif; ?> 
    <!-- start main content --> 
    <?php print($content) ?> 
    <!-- end main content --> 
  </div>



      </div><!-- main -->
    </td>
  </tr>
  <tr>
    <td colspan="3">
      <div>
    <?php if (is_array($secondary_links)): ?>
      <div id="secondary">
      <?php foreach (array_reverse($secondary_links) as $link): ?>
        <?php print $link; ?>
      <?php endforeach; ?>
      </div>
    <?php endif; ?>
      </div>
    </td>
  </tr>
  <tr>
    <td colspan="3">
      <div id="footer">
      <?php if ($footer_message) : ?> 
      <?php print $footer_message;?><br /> 
      <?php endif; ?> 
      </div>
    </td>
  </tr>
</table>
<?php print $closure;?> 
</body>
</html>