gworks.themeREADME.txt
Description------------
This initial posting is a quick reworking of gworks for 4.6 (it
was previously a 4.4 theme).  It now uses phptemplate instead of
xtemplate as a theme engine.  Most of the enhancements that were
in place in 4.4 - mainly, for image display - are gone, but may 
be added back in as time allows.

To use this theme, you need to edit the image file

themes/gworks/images/banner.png

to your desired look.